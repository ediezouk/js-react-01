import React from 'react';

import Box from './Box.react';

export default class Wrapper extends React.Component {

    constructor() {
        super();
    }

    render() {
        return (
            <div>
                <h1>Viditelnost nastavime pres props</h1>
                <Box canDisplay={true} />
            </div>
        );
    }
}
