import React from 'react';

export default class Counter extends React.Component {

    constructor() {
        super();
        this.state = {
            counter: 0
        };
    }

    handleClick = () => {
        this.setState({
            counter: this.state.counter + 1
        });
    }

    render() {
        return (
            <div>
                <p>Kliknul jsi {this.state.counter}x.</p>
                <button onClick={this.handleClick}>
                    Klikni!
                </button>
            </div>
        );
    }
}
