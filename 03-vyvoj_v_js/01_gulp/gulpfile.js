var gulp = require('gulp');
var ejs = require('gulp-ejs');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var stripDebug = require('gulp-strip-debug');
var minifyCSS = require('gulp-minify-css');
var minifyHTML = require('gulp-minify-html');
var rename = require('gulp-rename');
var argv = require('yargs').argv;


gulp.task('css', function() {
    return gulp.src('./src/assets/style.css')
        .pipe(minifyCSS())
        .pipe(gulp.dest('./dist/assets/'));
});

gulp.task('scripts', function() {
    return gulp.src('./src/assets/script.js')
        .pipe(stripDebug())
        .pipe(gulpIf(argv.env === 'prod', uglify()))
        .pipe(gulp.dest('./dist/assets/'));
});

gulp.task('html', function() {
    return gulp.src('./src/index.ejs')
        .pipe(ejs({
            'version': 2015
        }).on('error', gutil.log))
        .pipe(minifyHTML({empty: true, spare: true, quotes: true}))
        .pipe(rename('us.html'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('images', function() {
    return gulp.src('./src/assets/images/*.{png,jpg}')
        .pipe(gulp.dest('./dist/assets/img/'));
});

gulp.task('fonts', function() {
    return gulp.src('./src/assets/font/*.*')
        .pipe(gulp.dest('./dist/assets/font/'));
});

gulp.task('watch', function() {
    gulp.watch('./src/assets/*.js', ['scripts']);
    gulp.watch('./src/assets/style.css', ['css']);
    gulp.watch('./src/index.ejs', ['html']);
});

gulp.task('default', ['watch', 'scripts', 'css', 'html', 'images', 'fonts'], function() {});
