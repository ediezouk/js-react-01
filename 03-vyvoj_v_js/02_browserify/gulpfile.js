var gulp = require('gulp');
var browserify = require('gulp-browserify');

gulp.task('scripts', function() {
    return gulp.src('src/index.js')
        .pipe(browserify())
        .pipe(gulp.dest('./dist'))
});

gulp.task('default', ['scripts'], function() {});
