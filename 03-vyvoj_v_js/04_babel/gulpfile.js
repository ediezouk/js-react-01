var gulp       = require('gulp');
var browserify = require('browserify');
var source     = require('vinyl-source-stream');
var babelify   = require('babelify');


gulp.task('scripts', function() {
    return browserify({ entries: ['src/index.js'] })
        .transform(babelify)
        .bundle()
        .pipe(source('main.bundled.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['scripts'], function() {});
