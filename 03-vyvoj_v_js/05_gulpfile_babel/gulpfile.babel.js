import gulp from 'gulp';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import babelify from 'babelify';


gulp.task('scripts', () => {
    return browserify({ entries: ['src/index.js'] })
        .transform(babelify)
        .bundle()
        .pipe(source('main.bundled.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['scripts'], () => {});
