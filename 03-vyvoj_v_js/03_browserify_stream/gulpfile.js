var gulp       = require('gulp');
var browserify = require('browserify');
var source     = require('vinyl-source-stream');

gulp.task('scripts', function() {
    return browserify({ entries: ['src/index.js'] })
        .bundle()
        .pipe(source('main.bundled.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['scripts'], function() {});
