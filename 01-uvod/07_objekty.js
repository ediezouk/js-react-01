function A() {
    this.variable = 0;
}

A.prototype.add = function(num) {
    this.variable += num;
}

var obj = new A();

obj.add(5);
console.log(obj.variable);

console.log(obj.__proto__);
console.log(obj.__proto__.__proto__);
