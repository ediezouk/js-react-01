function foo() {
    console.log(this.a);
}

var obj = {
    a: 1
};

var obj2 = {
    a: 2
};

foo.call(obj);
foo.call(obj2);



function bar() {
    console.log(this.a, arguments);
}

bar.apply(obj, ['a']);
bar.apply(obj, ['b', 'c']);
