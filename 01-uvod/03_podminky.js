// truthy
if (true)
if ({})
if ([])
if (42)
if ("foo")


// falsy
if (false)
if (null)
if (undefined)
if (0)
if (NaN)
if ('')
