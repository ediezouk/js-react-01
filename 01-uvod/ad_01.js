// callback hell
function A(err, res) {
    // ...
    B(asdf, function(err, data) {
        // ...
        C(wer, function(err, d) {
            // ...
            // ...
            D(lkj, function(err, dat) {

            });
        });
    });
}

// 'reseni' v ES5
function A(err, res) {
    // ...
    B(asdf, cb1);
}

function cb1(err, data) {
    // ...
    C(wer, cb2);
}
// ...




// try/catch
try {
    doAsync( /*...*/ );
} catch(err) {
    /* ... */
}
