a;          // ReferenceError: a is not defined

var a;
a;          // undefined
typeof a;   // 'undefined'

a = null;
a;          // null
typeof a;   // 'object'

a = true;
a;          // true
typeof a;   // 'boolean'

a = 1;
a;          // 1
typeof a;   // 'number'

a = '';
a;          // ''
typeof a;   // 'string'

a = { value: 'asdf' };
a;          // Object {value: 'asdf'}
typeof a;   // 'object'

a = [];
a;          // []
typeof a;   // 'object'

a = function() { return 'ok'; }
typeof a;   // 'function'

var b = new a();
a;          // {}
a();        // 'ok'
typof a;    // 'function'
