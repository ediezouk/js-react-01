function foo() {
    console.log( this.a );
}

var obj = {
    a: 2,
    foo: foo
};

obj.foo(); // 2



var obj2 = {
    a: 2,
    foo: function() {
        console.log( this.a );
    }

};

obj2.foo(); // 2




function A() {
    this.variable = 5;
}

A();
console.log(variable);



function foo2(a) {
    this.a = a;
}

var bar = new foo2(3);
console.log( bar.a ); // 2
