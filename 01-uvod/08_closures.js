var addCount = function (msg) {
    var count = 0;
    return function() {
        count++;
        console.log(msg + count);
    };
};

var displayCounter = addCount('Number: ');
displayCounter();
