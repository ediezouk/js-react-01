var a = 1;  // globalni

function fn() {
    var a = 2;  // lokalni
    c = 4;      // globalni, prasarna
}
fn();

b = 3;      // globalni

window.a;   // 1
window.b;   // 3
c;          // 4
