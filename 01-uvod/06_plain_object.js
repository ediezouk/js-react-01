var obj = {
    a: 1,
    b: 'asdf',
    c: function() {
        console.log('c');
    }
}

for (key in obj) {
    console.log(key);
}

Object.keys(obj).forEach(function(key) {
    console.log(key);
})



var prefix = 'foo';

var obj2 = {
        [prefix + 'bar']: 1,
        [prefix + 'baz']: 2
}



var obj3 = {
    a: 1
}

('a' in obj3);  // true
