var arr = [
    {
        label: 'a',
        value: 1
    },
    {
        label: 'b',
        value: 2
    },
    {
        label: 'c',
        value: 3
    },
    {
        label: 'd',
        value: 4
    }
];

arr.forEach(function(el, index) {
    if (el.label == '') {
        throw new Error('empty label in [' + index + ']');
    }
});



arr.map(function(el) {
    return el.label;
})
// ['a', 'b', 'c', 'd']



function isBigEnough(value) {
  return value >= 10;
}
var filtered = [12, 5, 8, 130, 44].filter(isBigEnough);
// [12, 130, 44]
