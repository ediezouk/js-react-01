let obj = {
    0: 'a',
    1: 'b',
    2: 'c'
};

for (key in obj) {
    console.log(key);   // 1, 2, 3
}

for (val of obj) {
    console.log(val);   // 'a', 'b', 'c'
}
