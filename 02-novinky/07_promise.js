let doAsync = (url) => {
    return new Promise((resolve, reject) => {
        request
            .get(url)
            .send()
            .end((res, data) => {
                if (!!res) {
                    reject(err);
                } else {
                    resolve(JSON.parse(data.text));
                }
            });
    });
};

doAsync('//localhost')
    .then(data => {
        console.log(data);
    })
    .catch(err => {
        console.warn(err);
    })
