async function getAsyncData(url) {
    try {
        let data = await doAsync(url);

        return JSON.parse(data.text);
    } catch(err) {
        throw(err);
    }
}

// 1)
getAsyncData.then( /*...*/ );

// 2)
try {
    let data = await getAsyncData('//localhost');

    console.log(data);
} catch(err) {
    console.warn(err);
}
