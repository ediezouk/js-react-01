// rest
function sum(…numbers) {
    var result = 0;
    numbers.forEach(function(number) {
        result += number;
    });
    return result;
}

console.log(sum(1)); // 1
console.log(sum(1, 2, 3, 4, 5)); // 15



// spread
function sum(a, b, c) {
    return a + b + c;
}
var args = [1, 2];
console.log(sum(…args, 3)); // 6
