let property = Symbol();

class Something {
    constructor(){
        this[property] = 'test';
    }
}

let instance = new Something();

console.log(instance.property); // undefined
