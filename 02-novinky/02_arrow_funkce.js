// React 0.12.x
var CommentBox = React.createClass({
    handleClick() {
        console.log(this.props.title);
    },

    render() {
        return (
            <a href='//localhost' onClick={this.handleClick.bind(this)}>Odkaz</a>
        );
    }
});

// React 0.13.x
class CommentBox extends React.Component {
    handleClick = () => {
        console.log(this.props.title);
    }

    render() {
        return (
            <a href='//localhost' onClick={this.handleClick}>Odkaz</a>
        );
    }
}
