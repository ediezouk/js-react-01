let PrivatePerson = (function(name) {
    /*
     * @hidden
    */
    var _name = '';

    /*
     * @class PrivatePerson
    */
    return new class {
        constructor() {
            _name = name;
        }

        getName() {
            return _name;
        }

        setName(name) {
            _name = name;
        }
    }();
});
