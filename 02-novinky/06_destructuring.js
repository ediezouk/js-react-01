function A() {
    let err = null;
    let data;

    // ...

    return {
        err: err,
        data: data
    };
}

let { err, data } = A();
