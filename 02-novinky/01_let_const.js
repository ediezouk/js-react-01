// ES5
var a = 1;

if (1 === a) {
    var b = 2;
}

for (var c = 0; c < 3; c++) {
    // …
}

b;  // 2
c;  // 3



let d = 1;

if (1 === d) {
 let e = 2;
}

d;  // reference error
