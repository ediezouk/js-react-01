// ES5
function inc(number, increment) {
    increment = increment || 1;
    return number + increment;
}

// ES6
function inc(number, increment = 1) {
    return number + increment;
}




function sum(a, b = 2, c) {
  return a + b + c;
}
console.log(sum(1, 5, 10));         // 16 -> b === 5
console.log(sum(1, undefined, 10)); // 13 -> b as default
